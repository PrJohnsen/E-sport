#!/usr/bin/env python3
# coding utf-8

import json

"""
json example cf datasets/articles.json

"""

def read_json(myfile):
	#ouvrir le fichier
	with open(myfile, "r") as f:
		#je stocke dans ma variable
		data = json.load(f)
		#j'affiche les clés de mon dictionnaire
		print(len(data["articles"]))

if __name__ == "__main__":
	read_json("articles.json")